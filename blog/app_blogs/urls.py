from django.urls import path
from django.contrib.auth import views as auth_login
from . import views

urlpatterns = [
    #Página principal
    path('', views.index, name='index'),

    #Novo post no blog
    path('novo_post/', views.novo_post, name='novo_post'),

    #Editar post no blog
    path('editar_post/<int:idpost>/', views.editar_post, name='editar_post'),



]
