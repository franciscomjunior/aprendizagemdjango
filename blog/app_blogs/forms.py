from django import forms
from .models import BlogPost


class PostForm(forms.ModelForm):
    """Formulário de post"""

    class Meta:
        model = BlogPost
        fields = ['title', 'text']
        labels = {'title': 'Título do Post', 'text': 'Post'}
        widgets = {'text': forms.Textarea(attrs={'cols': 80})}
