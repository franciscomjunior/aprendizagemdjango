from django.contrib import admin

from app_blogs.models import BlogPost

admin.site.register(BlogPost)
