from django.db import models


class BlogPost(models.Model):
    """Posts no blog"""
    title = models.CharField(max_length=200)
    text = models.TextField()
    date_add = models.DateField(auto_now_add=True)

    class Meta:
        """Altera o nome da classe"""
        verbose_name_plural = 'posts'

    def __str__(self):
        """Apresenta uma representação da classe em String"""
        return self.title
