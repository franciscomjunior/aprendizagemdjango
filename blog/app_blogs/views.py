from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

from .models import BlogPost
from .forms import PostForm

def index(request):
    """Página principal do blog"""
    posts = BlogPost.objects.all()
    context = {
        'posts': posts,
    }
    return render(request, 'app_blogs/index.html', context)

def novo_post(request):
    """Inserir novo post"""
    if request.method != 'POST':
        """Nenhum dado submetido. Apresenta formulário em branco"""
        form = PostForm
    else:
        """Dados de POST submetidos. Processa os dados"""
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('index'))
    context = {'form': form}
    return render(request, 'app_blogs/novo_post.html', context)

def editar_post(request, idpost):
    """Edita post específico"""
    post = BlogPost.objects.get(id=idpost)

    if request.method != 'POST':
        """Não tendo dados para processar, retorna um formulário vazio"""
        form = PostForm(instance=post)
    else:
        """Dados para processar"""
        form = PostForm(instance=post, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('index'))
    context = {
        'post': post,
        'form': form,
    }
    return render(request, 'app_blogs/editar_post.html', context)





