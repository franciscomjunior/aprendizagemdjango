from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import UserCreationForm

def logout_view(request):
    """Faz logout do usuário"""
    logout(request)
    return HttpResponseRedirect(reverse('index'))

def cadastro(request):
    """Cadastra novos usuários"""
    if request.method != 'POST':
        form = UserCreationForm()
    else:
        #Processa o formulário preenchido
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            novo_usuario = form.save()
            #Faz o login do usuário e redireciona para a página inicial
            usuario_autenticado = authenticate(username=novo_usuario.username,
                                               password=request.POST['password1'])
            login(request, usuario_autenticado)
        return HttpResponseRedirect(reverse('index'))
    context = {
        'form': form,
    }
    return render(request, 'app_users/cadastro.html', context)

