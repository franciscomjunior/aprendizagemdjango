from django.urls import path
from django.contrib.auth import views as auth_login
from . import views

urlpatterns = [
    path('login/', auth_login.LoginView.as_view(template_name='app_users/login.html'), name='login'),
    path('logout/', views.logout_view, name='logout_view'),
    path('cadastro/', views.cadastro, name='cadastro'),
]